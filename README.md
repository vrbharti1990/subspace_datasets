# Data repo for subspace clustering datasets

Repo has two directories:

* real\_datasets: Contains the [anuran calls dataset](https://archive.ics.uci.edu/ml/datasets/Anuran+Calls+%28MFCCs%29) and the extracted features for the image datasets ([coil-100](https://www.cs.columbia.edu/CAVE/software/softlib/coil-100.php), [fruits](https://www.kaggle.com/moltean/fruits/version/2), and [lego](https://www.kaggle.com/joosthazelzet/lego-brick-images))

* synthetic\_datasets: Consists of 10 axis parallel and 10 arbitrarily oriented synthetic datasets. These datasets have both row and column format. In row format data, each row is a data point while in column format each column is a data point.
